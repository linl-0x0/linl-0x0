### hey there <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">
hi, i'm [linl](https://github.com/linl-0x0), a passionate self-taught full stack engineer. My passion for software is to come up with ideas and implement them through elegant interfaces. I pay close attention to the experience, architecture, and code quality of what I'm building.
I am also an open source enthusiast and maintainer. I have learned a lot from the open source community and I love the way collaboration and knowledge sharing are achieved through open source.
I'm also in business,This is my personal storage unit after I reset it  

-  🔭 **[database](https://github.com/linl-0x0/database)**：Knowledge base::database

<br/>

**learning route(Around the `Linux` direction)**

|        name         | state | Documentation | Video | e-book |
| :-----------------: | :---: | :-----------: | :---: | :----: |
| **`Git版本控制器`** |   ✅   |       ✅       |   ❌   |   ✅    |
|     **`数学`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`英语`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`俄语`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|   **`操作系统`**    |   ❌   |       ❌       |   ❌   |   ❌    |
|  **`计算机网络`**   |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`Linux`**     |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`Bash`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|    **`Python`**     |   ❌   |       ❌       |   ❌   |   ❌    |
|    **`Golang`**     |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`Java`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|       **`C`**       |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`AT&T`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|   **`信息安全`**    |   ❌   |       ❌       |   ❌   |   ❌    |
|     **`物理`**      |   ❌   |       ❌       |   ❌   |   ❌    |
|         ...         |       |               |       |        |

<br/>

**languages and tools:**  

<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/vue/vue.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/react/react.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/5c058a388828bb5fde0bcafd4bc867b5bb3f26f3/topics/graphql/graphql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/cpp/cpp.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/firebase/firebase.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png"></code>

<p align="center"> <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=linl-0x0&langs_count=compact&theme=dark" alt="abhisheknaiidu" />
<br/>

📈 **my github stats**

<p align="center"> <img src="https://github-readme-stats.vercel.app/api?username=linl-0x0&show_icons=true&theme=dark" alt="linl" />
